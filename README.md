# type-theory-recommendations

Some recommendations on papers, books or guides to read about Type Theory.

Nothing here is ordered, so check out as much stuff as possible and try to (slowly) build a proper understanding of the topic. This area is very, very broad and quite useful.

**Books:**

- Types and Programming Languages - Benjamin Pierce. The usual recommended book.
- Practical Foundations for Programming Languages (Second Edition) - Robert Harper, chapters 2-25.
- Type Theory and Functional Programming - Simon Thompson. More logic focused.
- Programming in Martin-Löf's Type Theory - Bengt Nordström, Kent Petersson, Jan M Smith. Some more applications and examples.

**Essential papers:**

- The principal type-scheme of an object in combinatory logic - Robert Hindley. Requires some reading on combinatory logic first.
- A theory of type polymorphism in programming - Robin Milner.
- Principal type-schemes for functional programs - Luis Damas, Robin Milner. The most popular type system

**Extra recommendations:**

Those are mostly just to expand on extra useful topics, note that there are many papers explaining other novel ways to do type systems and more cool stuff.

(Also they are just papers)

- Strict Bidirectional Type Checking - Adam Chlipala, Leaf Petersen, Robert Harper. Another way to do type systems, sometimes used since it is more expressive.
- TALx86: A Realistic Typed Assembly Language - Morriset, Crary, Glew, Grossman and many others. No typing judgements but a nice overview.
- The Design and Implementation of Typed Scheme - Sam Tobin-Hochstadt, Matthias Felleisen. Union types, latent predicates and all the machinery required to make Scheme, Python, JS and others statically typed while keeping most of the idioms of those languages.
- A Polymorphic Type System for Extensible Records and Variants - Benedict Gaster, Mark Jones. Essentially a much, much better version of structs and unions. Gives typing rules, unification rules, inference rules, compilation techniques and more.
- Practical type inference for arbitrary-rank types - Peyton Jones, Vytiniotis, Weirich, Shields. One of the many approaches to rank N polymorphism (AKA putting foralls anywhere), great introduction, shows an example of syntax-directed inference rules, which I still don't understand (the non-syntax-directed ones are the usual ones).
- HMF: Simple Type Inference for First-Class Polymorphism - Daan Leijen. Another attempt at rank N polymorphism. Note: It is not as simple as it says on the title.
- Guarded Impredicative Polymorphism, extended version - Serrano, Hage, Vytiniotis, Peyton Jones. Another one, this one compares other methods and showcases some fancy type system creation tools.
- Complete and Easy Bidirectional Typechecking for Higher-Rank Polymorphism - Dunfield, Krishnaswami. Yet another one, look at the comparison tables (this one is not impredicative, for example).
- Generalizing Hindley-Milner Type Inference Algorithms - Heeren, Hage, Swierstra. Gives a generalization of algorithm W and M (the main ones for Hindley-Damas-Milner) with (possibly) nicer error messages.
- Wobbly types: type inference for generalized algebraic data types - Peyton Jones, Washburn, Weirich. Essentially "pattern matching on some constructors makes the inferred type more specific", useful for interpreters.
- Dependent Types: Easy as PIE - Vytiniotis, Weirich. An introduction to dependent types, it lacks definitional equality and type erasure but it is fairly nice.

**New and complicated, straight out of current research:**

- Perceus: Garbage Free Reference Counting with Reuse - Rinking, Xie, de Moura, Leijen. Essentially what Rust does with the borrow checker but fully inferred, pretty much "same/better performance as if I managed the memory without managing memory at all". Also talks about linear types. Fairly complex.
- FP2: Fully in-place Functional Programming - Lorenzen, Leijen, Swierstra. Insane and very-new optimizations for functional programming languages to make them reach near C speed (assuming usual techniques, not ultra-optimization) in microbenchmarks (previously only on high level code). Combine this with the previous one and go zoomin'.

**Bonus**:

Many other type systems can be found when you search for:

- Infinite/recursive types
- Algebraic effects
- Polymorphic records/variants
- Refinement types
- Subtyping
- Linear types
- Dependent types
- Cedille, Agda, Lean, Coq or any other modern theorem prover. Most importantly, look at the typing rules of the core systems.
- Lambda Cube
